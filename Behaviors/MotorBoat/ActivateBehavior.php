<?php
namespace Behaviors\MotorBoat;

use IBehavior;

class ActivateBehavior implements IBehavior{
    public function do()
    {
        echo "завести мотор", PHP_EOL;
    }
}