<?php

interface IFormatOutput {
    public function format(Closure $action);
}