<?php

require_once("./AppLogic/TransportModel.php");
require_once("./Behaviors/Bicycle/UnblockBehavior.php");
require_once("./Behaviors/Bicycle/ActivateBehavior.php");
require_once("./Behaviors/Bicycle/RunBehavior.php");

use Behaviors\Bicycle\RunBehavior;
use Behaviors\Bicycle\UnblockBehavior;
use Behaviors\Bicycle\ActivateBehavior;



class Bicycle extends TransportModel {

    public function __construct()
    {
        echo "стратегия Bicycle:", PHP_EOL;
        $this->unblocBehavior = new UnblockBehavior();
        $this->activateBehavior = new ActivateBehavior();
        $this->runBehavior = new RunBehavior();
    }

}