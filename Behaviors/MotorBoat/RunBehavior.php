<?php
namespace Behaviors\MotorBoat;

use IBehavior;

class RunBehavior implements IBehavior{
    public function do()
    {
        echo "Выставить скорость на движение вперед", PHP_EOL;
    }
}