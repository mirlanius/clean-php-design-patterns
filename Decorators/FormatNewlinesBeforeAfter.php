<?php
require_once("./Decorators/IFormatOut.php");
 class FormatNewlinesBeforeAfter implements IFormatOutput {

    public function format(Closure $action)
    {        
        $action();
        echo PHP_EOL;
    }

 }