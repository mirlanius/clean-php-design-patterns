<?php

require_once("./Behaviors/IBehavior.php");
require_once("./Decorators/IFormatOut.php");

/**
 * Абстрактный класс с бизнес логикой приложения
 */
abstract class TransportModel {

    protected IBehavior $unblocBehavior;
    protected IBehavior $activateBehavior;
    protected IBehavior $runBehavior;

    
    public function doUnblock(){
        $this->unblocBehavior->do();
    }

    public function doActivate(){
        $this->activateBehavior->do();
    }

    public function doRun(){
        $this->runBehavior->do();
    }


}