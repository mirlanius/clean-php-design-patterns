<?php

require_once("./Decorators/FormatNewlinesBeforeAfter.php");


/**
 * Класс приложения реализующий логику бизнес модели
 */
class App {

    private TransportModel $model;

    public function __construct(TransportModel $model)
    {
        $this->model = $model;
    }

    public function start(){
        $this->model->doUnblock();
        $this->model->doActivate();
        $this->model->doRun();
    }

}