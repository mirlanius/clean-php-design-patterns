<?php

interface IActivateBehavior {
    public function activate();
}