<?php
namespace Behaviors\Bicycle;

use IBehavior;

class ActivateBehavior implements IBehavior{
    public function do()
    {
        echo "выровнять баланс", PHP_EOL;
    }
}