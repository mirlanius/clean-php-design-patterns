<?php
 interface IBehavior {
    public function do();
 }