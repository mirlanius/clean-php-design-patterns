<?php 

require_once("./App.php");
require_once("./Strategies/Bicycle.php");
require_once("./Strategies/MotorBoat.php");
require_once("./Strategies/Car.php");


//декоратор для форматирования вывода блоков текста
$decorator = new FormatNewlinesBeforeAfter();

//реализация логики велосипеда
$model = new App(new Bicycle());
$decorator->format(function() use ($model){$model->start();});

//реализация логики моторной лодки
$model = new App(new MotorBoat());
$decorator->format(function() use ($model){$model->start();});

//реализация логики машины
$model = new App(new Car());
$decorator->format(function() use ($model){$model->start();});

