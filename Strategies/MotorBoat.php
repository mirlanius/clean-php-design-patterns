<?php

require_once("./AppLogic/TransportModel.php");
require_once("./Behaviors/MotorBoat/UnblockBehavior.php");
require_once("./Behaviors/MotorBoat/ActivateBehavior.php");
require_once("./Behaviors/MotorBoat/RunBehavior.php");

use Behaviors\Bicycle\RunBehavior;
use Behaviors\Bicycle\UnblockBehavior;
use Behaviors\Bicycle\ActivateBehavior;


class MotorBoat extends TransportModel {

    public function __construct()
    {
        echo "стратегия Motor Boat:", PHP_EOL;
        $this->unblocBehavior = new UnblockBehavior();
        $this->activateBehavior = new ActivateBehavior();
        $this->runBehavior = new RunBehavior();
    }

}