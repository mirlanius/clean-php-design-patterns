<?php
namespace Behaviors\Car;

use IBehavior;

class RunBehavior implements IBehavior{
    public function do()
    {
        echo "Нажать на педаль газа", PHP_EOL;
    }
}