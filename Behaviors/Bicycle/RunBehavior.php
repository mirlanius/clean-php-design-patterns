<?php
namespace Behaviors\Bicycle;

use IBehavior;

class RunBehavior implements IBehavior{
    public function do()
    {
        echo "крутить педали", PHP_EOL;        
    }
}