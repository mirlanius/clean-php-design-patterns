<?php

interface IRunBehavior {
    public function run();
}