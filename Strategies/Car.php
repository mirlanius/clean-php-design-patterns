<?php

require_once("./AppLogic/TransportModel.php");
require_once("./Behaviors/Car/UnblockBehavior.php");
require_once("./Behaviors/Car/ActivateBehavior.php");
require_once("./Behaviors/Car/RunBehavior.php");

use Behaviors\Car\RunBehavior;
use Behaviors\Car\UnblockBehavior;
use Behaviors\Car\ActivateBehavior;


class Car extends TransportModel {

    public function __construct()
    {
        echo "стратегия Car:", PHP_EOL;
        $this->unblocBehavior = new UnblockBehavior();
        $this->activateBehavior = new ActivateBehavior();
        $this->runBehavior = new RunBehavior();
    }

}